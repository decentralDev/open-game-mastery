# Player Note Generator 
This tool will take input (currently English only at the moment) and do a transliteration into a font selected by the game master.
Currently debating on libreOffice plugin or python?(if possible), need to learn either way

## Input 
Note to player in non-marked up text form (love to add marked text later)
Transliterated Language Selected

## Output
2 separate pages
	1 with typed text (for transliteration)
	1 with new language text (for giving to players)

### Wish List
* Get it working for any language
* Add markdown text capability
