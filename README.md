# open-game-mastery

Free tools to be used by game masters for making their world come alive.

## Wish List of Tools to be Implemented
* Player Note Generator - Type common language and it spits out common language with substituted alphabet
* Combat Tracker - Virtual Tool and .ODT
* templates for world building - either ODT or mad libs type prompt
* Dir for tables

Don't see a feature you would like in your game? Feel free to add it here or file an issue to request it.
